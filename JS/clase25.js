class Persona {
    constructor(nombre, apellido, altura){
       this.nombre = nombre
       this.apellido = apellido
       this.altura = altura
     }

     saludar(){
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
     }

     soyAlto(){
        return this.altura > 1.8
     }
}

class Desarrollador extends Persona{
    constructor(nombre,apellido, altura){
        super(nombre,apellido, altura)
    }

    saludar(){
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy desarrollador/a`)
    }
}


function Desarrollador(nombre,apellido, altura){
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}


Desarrollador.prototype.saludar = function (){
}



/*
var adri = new Persona('Adrián', 'Antañón', 1.88)
var andrea = new Persona('Andrea', 'Morcillo', 1.58)*/
