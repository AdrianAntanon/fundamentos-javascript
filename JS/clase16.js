var estudios = prompt('¿Qué nivel de educación posees?').toLowerCase()
var salario = 1000
var aumento = 100

switch(estudios){
    case 'primaria':
        console.log(`Salario medio ${salario} €`)
        break
    case 'secundaria':
        salario += aumento
        console.log(`Salario medio ${salario} €`)
        break
    case 'batxillerato':
    case 'bachillerato':
        salario += aumento*2
        console.log(`Salario medio ${salario} €`)
        break
    salario += aumento
    case 'grado medio':
    case 'cfgm':
        salario += aumento*3
        console.log(`Salario medio ${salario} €`)
        break
    case 'grado superior':
    case 'cfgs':
        salario += aumento*4
        console.log(`Salario medio ${salario} €`)
        break
    case 'universitarios':
    case 'diplomatura':
    case 'licenciatura':
        salario += aumento*5
        console.log(`Salario medio ${salario} €`)
        break
    default:
        console.log('No ex una opción válida')
}