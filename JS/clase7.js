var adri = {
    nombre: 'Adrián',
    apellido: 'Antañón',
    edad: 26
}

var alex = {
    nombre: 'Alejandro',
    apellido: 'González',
    edad: 25
}

//function imprimirNombreMayus({nombre}){
//    console.log(nombre.toUpperCase())
//}

function imprimirNombreMayus(persona){
// var nombre = persona.nombre
var {nombre} = persona
    console.log(nombre.toUpperCase())
}
imprimirNombreMayus(adri)
imprimirNombreMayus(alex)
/*imprimirNombreMayus({nombre: 'Gabriel'})*/


function imprimirNombreYEdad({nombre, edad}){
    console.log('Hola, me llamo ' + nombre + ' y tengo ' + edad + ' años')
}

imprimirNombreYEdad(alex)
imprimirNombreYEdad(adri)

function cumpleanos(persona){
    return{
        ...persona,
        edad: persona.edad+1
    }
}

