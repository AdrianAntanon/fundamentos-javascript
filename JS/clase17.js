var adri ={
    nombre: 'Adrián',
    apellido: 'Antañón',
    altura: 188
}

var andrea = {
    nombre: 'Andrea',
    apellido: 'Morcillo',
    altura: 158
}

var joel = {
    nombre: 'Joel',
    apellido: 'Antañón',
    altura: 179
}

var personas = [adri, andrea, joel]

for(var i=0;i<personas.length;i++){
    var persona = personas[i]

    console.log(`${persona.nombre} mide ${persona.altura} centímetros`)
}