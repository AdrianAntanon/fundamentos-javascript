var adri = {
    nombre: 'Adri',
    apellido: 'Antañón',
    edad: 26,
    informatico: true,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: false,
    precavido: true
}

function imprimirProfesiones(persona){
    console.log(`${persona.nombre} es:`)


    if(persona.informatico){
        console.log('Informático')
    }else{
        console.log('Un estudiante de desarrollo web')
    }

    if(persona.cocinero){
        console.log('Cocinero')
    }

    if(persona.cantante){
        console.log('Cantante')
    }

    if(persona.precavido){
        console.log('Una persona precavida')
    }
}

imprimirProfesiones(adri)

function imprimirMayorEdad(persona){
    console.log(`${persona.nombre} es: `)

    if(persona.edad <18){
        console.log('menor de edad')
    }else{
        console.log('mayor de edad')
    }
}

imprimirMayorEdad(adri)