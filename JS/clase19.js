var adri ={
    nombre: 'Adrián',
    apellido: 'Antañón',
    altura: 1.88
}

var andrea = {
    nombre: 'Andrea',
    apellido: 'Morcillo',
    altura: 1.58
}

var joel = {
    nombre: 'Joel',
    apellido: 'Antañón',
    altura: 1.79
}

const ES_ALTA = ({altura}) => altura > 1.79
const ES_BAJA = ({altura}) => altura < 1.80

var personas = [adri, andrea, joel]

var personasAltas = personas.filter(ES_ALTA)
var personasBajas = personas.filter(ES_BAJA)

const PASAR_ALTURA_A_CMS = persona => ({
    ...persona,
    altura: persona.altura * 100
    })

var personasCms = personas.map(PASAR_ALTURA_A_CMS)

console.log(personasCms)
console.log(personas)