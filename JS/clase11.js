var adri ={
    nombre: 'Adri',
    apellido: 'Antañón',
    edad: 26
}

var joel ={
    nombre: 'Joel',
    apellido: 'Antañón',
    edad: 17
}

const MAYORIA_DE_EDAD = 18

function EsMayorDeEdad(persona) {
    return persona.edad >= MAYORIA_DE_EDAD
}

function imprimirSiEsMayorDeEdad(persona){

    if(EsMayorDeEdad(persona)){
        console.log(`${persona.nombre} es mayor de edad`)
    }else{
        console.log(`${persona.nombre} es menor de edad`)
    }
}

imprimirSiEsMayorDeEdad(adri)
imprimirSiEsMayorDeEdad(joel)