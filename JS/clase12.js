var adri ={
    nombre: 'Adri',
    apellido: 'Antañón',
    edad: 26
}

var joel ={
    nombre: 'Joel',
    apellido: 'Antañón',
    edad: 17
}

const MAYORIA_DE_EDAD = 18

/*
const esMayorDeEdad = function (persona) {
    return persona.edad >= MAYORIA_DE_EDAD
}*/

const EsMayorDeEdad = ({edad}) => edad >= MAYORIA_DE_EDAD
const EsMenorDeEdad = ({edad}) => edad < MAYORIA_DE_EDAD

function imprimirSiEsMayorDeEdad(persona){

    if(EsMayorDeEdad(persona)){
        console.log(`${persona.nombre} es mayor de edad`)
    }else{
        console.log(`${persona.nombre} es menor de edad`)
    }
}

function permitirAcceso(persona){
    if(!EsMayorDeEdad(persona)){
        console.log(`${persona.nombre} acceso denegado`)
    }else{
        console.log(`${persona.nombre} acceso permitido`)
    }
}

imprimirSiEsMayorDeEdad(adri)
imprimirSiEsMayorDeEdad(joel)
permitirAcceso(adri)
permitirAcceso(joel)