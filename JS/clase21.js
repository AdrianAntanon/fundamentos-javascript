function Persona(nombre, apellido, altura){
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}

Persona.prototype.saludar = function(){
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
}

Persona.prototype.soyAlto = function (){
    if(this.altura >= 180){
        console.log(`${this.nombre} es alto, ya que mide ${this.altura} centímetros`)
    }else{
        console.log(`${this.nombre} no es alta, ya que mide ${this.altura} centímetros`)
    }
}

var adri = new Persona('Adrián', 'Antañón', 188)
var andrea = new Persona('Andrea', 'Morcillo', 158)