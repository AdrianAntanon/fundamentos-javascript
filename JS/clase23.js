function Persona(nombre, apellido, altura){
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}

Persona.prototype.saludar = function(){
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
}

Persona.prototype.soyAlto = function (){

    return this.altura > 1.8
}

var adri = new Persona('Adrián', 'Antañón', 1.88)
var andrea = new Persona('Andrea', 'Morcillo', 1.58)