var adri = {
    nombre: 'Adri',
    apellido: 'Antañón',
    edad: 26,
    peso: 75
}

const DIAS_TOTALES_AÑO = 365
const CAMBIO_PESO = 0.2

console.log(`Al inicio del año ${adri.nombre} pesa ${adri.peso}kg`)

const aumentarDePeso = persona => persona.peso += CAMBIO_PESO
const bajarDePeso = persona => persona.peso -= CAMBIO_PESO


for(var i=1; i<=DIAS_TOTALES_AÑO; i++){
    var random = Math.random()

    if(random < 0.25){
        aumentarDePeso(adri)
    }else if(random < 0.50){
        bajarDePeso(adri)
    }
}


console.log(`Al final del año ${adri.nombre} pesa ${adri.peso.toFixed(1)}kg`)
