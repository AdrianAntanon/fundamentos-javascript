var adri = {
    nombre: 'Adri',
    apellido: 'Antañón',
    edad: 26,
    peso: 75
}

const DIAS_TOTALES_AÑO = 365
const CAMBIO_PESO = 0.3
const comeMucho = () => Math.random() < 0.3
const realizaDeporte = () => Math.random() < 0.4

const META = adri.peso -3
var dias = 0


const aumentarDePeso = persona => persona.peso += CAMBIO_PESO
const bajarDePeso = persona => persona.peso -= CAMBIO_PESO

while(adri.peso > META){

    if(comeMucho()){
        aumentarDePeso(adri)
    }
    if(realizaDeporte()){
        bajarDePeso(adri)
    }

    dias += 1
}

console.log(`Pasaron ${dias} días hasta que ${adri.nombre} adelgazó 3kg`)

