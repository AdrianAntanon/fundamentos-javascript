var adri ={
    nombre: 'Adrián',
    apellido: 'Antañón',
    altura: 188
}

var andrea = {
    nombre: 'Andrea',
    apellido: 'Morcillo',
    altura: 158
}

var joel = {
    nombre: 'Joel',
    apellido: 'Antañón',
    altura: 179
}

const ES_ALTA = ({altura}) => altura > 179
const ES_BAJA = ({altura}) => altura < 180

var personas = [adri, andrea, joel]

var personasAltas = personas.filter(ES_ALTA)
var personasBajas = personas.filter(ES_BAJA)

console.log(personasAltas)
console.log(personasBajas)